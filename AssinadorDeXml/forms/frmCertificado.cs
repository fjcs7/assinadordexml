﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using AssinadorDeXml.utils;
using AssinadorDeXml.utils.objetos;
using static AssinadorDeXml.utils.objetos.AssinadorConfig;

namespace PDVNFCe.Forms
{
    public partial class frmConfigCertificados : Form, IDisposable
    {

        public frmConfigCertificados()
        {
            InitializeComponent();
            Limpar();
        }

        private void frmConfigNFCe_Load(object sender, EventArgs e)
        {
            CarregaConfiguracao();

            txtSerialCert.Focus();
        }

        private void Limpar()
        {
            txtSerialCert.Text = "";
            txtCdPIN.Text = "";
            txtCaminhoCert.Text = "";
            txtSenhaCert.Text = "";
        }

        private void CarregaConfiguracao()
        {
            try
            {
                var configXml = Funcoes.GetAssinadorConfig();
                if (configXml != null)
                {
                    txtSerialCert.Text = configXml.Config.Instalado.NumeroSerialCertificado;
                    txtCdPIN.Text = configXml.Config.Instalado.SenhaCertificado;

                    txtCaminhoCert.Text = configXml.Config.Arquivo.CaminhoCertificado;
                    txtSenhaCert.Text = configXml.Config.Arquivo.SenhaCertificado;
                }
            }
            catch (Exception ex)
            {
                Funcoes.Critica(ex.Message, ex);
            }
        }

        private void txtSerialCert_Enter(object sender, EventArgs e)
        {
            txtSerialCert.SelectAll();
        }

        private void txtCdPIN_Enter(object sender, EventArgs e)
        {
            txtCdPIN.SelectAll();
        }

        private void btnPesqCert_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            frmCertificados fCertificados = new frmCertificados();
            fCertificados.ShowDialog(this);

            this.Enabled = true;

            if (fCertificados.Certificado != "")
                txtSerialCert.Text = fCertificados.Certificado;


        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                AssinadorConfig cnfg = new AssinadorConfig();
                cnfg.Config.Arquivo = new DadosArquivo()
                                        {
                                            CaminhoCertificado = txtCaminhoCert.Text,
                                            SenhaCertificado = txtSenhaCert.Text
                                        };
                cnfg.Config.Instalado = new DadosInstalados()
                                        {
                                            NumeroSerialCertificado = txtSerialCert.Text,
                                            SenhaCertificado = txtCdPIN.Text
                                        };
                Funcoes.SetConfig(cnfg);
                this.Cursor = Cursors.Default;
                this.Close();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Funcoes.Critica(ex.Message, ex);
            }
        }

        private bool Valida()
        {
            //X509Certificate2 DadosCert;
            //string strDtFim = "";

            if (txtCaminhoCert.Text != null && txtCaminhoCert.TextLength > 0)
            {
                if (txtSenhaCert.Text.Length == 0 || string.IsNullOrEmpty(txtSenhaCert.Text.Trim()))
                {
                    //clsFuncoes.Advertencia("A senha para abertura do arquivo deve ser informada.");
                    txtSenhaCert.Focus();
                    return false;

                }
            }

            try
            {

            }
            catch (Exception e)
            {
                Funcoes.Critica(e.Message);
                return false;
            }

            return true;
        }

        void IDisposable.Dispose()
        {
            this.Dispose(true);
        }

        private void btnLocalArq_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "All Files|*.pfx; *.p12";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtCaminhoCert.Text = openFileDialog1.FileName;
            }
        }

        private void txtSerialCert_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
