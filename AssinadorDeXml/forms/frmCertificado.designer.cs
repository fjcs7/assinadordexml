﻿namespace PDVNFCe.Forms
{
    partial class frmConfigCertificados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TabControl tabCertificado;
            this.tabInstalado = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCdPIN = new System.Windows.Forms.TextBox();
            this.btnPesqCert = new System.Windows.Forms.Button();
            this.txtSerialCert = new System.Windows.Forms.TextBox();
            this.tabArquivo = new System.Windows.Forms.TabPage();
            this.lblSenhaArquivo = new System.Windows.Forms.Label();
            this.txtSenhaCert = new System.Windows.Forms.TextBox();
            this.btnLocalArq = new System.Windows.Forms.Button();
            this.lblCaminhoCert = new System.Windows.Forms.Label();
            this.txtCaminhoCert = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            tabCertificado = new System.Windows.Forms.TabControl();
            tabCertificado.SuspendLayout();
            this.tabInstalado.SuspendLayout();
            this.tabArquivo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCertificado
            // 
            tabCertificado.Controls.Add(this.tabInstalado);
            tabCertificado.Controls.Add(this.tabArquivo);
            tabCertificado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            tabCertificado.HotTrack = true;
            tabCertificado.Location = new System.Drawing.Point(8, 8);
            tabCertificado.Name = "tabCertificado";
            tabCertificado.SelectedIndex = 0;
            tabCertificado.Size = new System.Drawing.Size(306, 132);
            tabCertificado.TabIndex = 14;
            // 
            // tabInstalado
            // 
            this.tabInstalado.BackColor = System.Drawing.Color.Transparent;
            this.tabInstalado.Controls.Add(this.label2);
            this.tabInstalado.Controls.Add(this.label1);
            this.tabInstalado.Controls.Add(this.txtCdPIN);
            this.tabInstalado.Controls.Add(this.btnPesqCert);
            this.tabInstalado.Controls.Add(this.txtSerialCert);
            this.tabInstalado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabInstalado.ForeColor = System.Drawing.Color.Navy;
            this.tabInstalado.Location = new System.Drawing.Point(4, 26);
            this.tabInstalado.Name = "tabInstalado";
            this.tabInstalado.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabInstalado.Size = new System.Drawing.Size(298, 102);
            this.tabInstalado.TabIndex = 0;
            this.tabInstalado.Text = "Instalado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "PIN certificado A3* (Senha Certificado)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 16);
            this.label1.TabIndex = 13;
            this.label1.Text = "N° serial certificado";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCdPIN
            // 
            this.txtCdPIN.BackColor = System.Drawing.Color.White;
            this.txtCdPIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCdPIN.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCdPIN.ForeColor = System.Drawing.Color.Navy;
            this.txtCdPIN.Location = new System.Drawing.Point(16, 71);
            this.txtCdPIN.MaxLength = 50;
            this.txtCdPIN.Name = "txtCdPIN";
            this.txtCdPIN.Size = new System.Drawing.Size(268, 22);
            this.txtCdPIN.TabIndex = 17;
            this.txtCdPIN.UseSystemPasswordChar = true;
            // 
            // btnPesqCert
            // 
            this.btnPesqCert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesqCert.ForeColor = System.Drawing.Color.Navy;
            this.btnPesqCert.Location = new System.Drawing.Point(254, 27);
            this.btnPesqCert.Name = "btnPesqCert";
            this.btnPesqCert.Size = new System.Drawing.Size(30, 24);
            this.btnPesqCert.TabIndex = 15;
            this.btnPesqCert.Text = "...";
            this.btnPesqCert.UseVisualStyleBackColor = true;
            this.btnPesqCert.Click += new System.EventHandler(this.btnPesqCert_Click);
            // 
            // txtSerialCert
            // 
            this.txtSerialCert.BackColor = System.Drawing.Color.White;
            this.txtSerialCert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialCert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerialCert.ForeColor = System.Drawing.Color.Navy;
            this.txtSerialCert.Location = new System.Drawing.Point(16, 28);
            this.txtSerialCert.MaxLength = 50;
            this.txtSerialCert.Name = "txtSerialCert";
            this.txtSerialCert.Size = new System.Drawing.Size(234, 22);
            this.txtSerialCert.TabIndex = 14;
            this.txtSerialCert.TextChanged += new System.EventHandler(this.txtSerialCert_TextChanged);
            // 
            // tabArquivo
            // 
            this.tabArquivo.BackColor = System.Drawing.Color.Transparent;
            this.tabArquivo.Controls.Add(this.lblSenhaArquivo);
            this.tabArquivo.Controls.Add(this.txtSenhaCert);
            this.tabArquivo.Controls.Add(this.btnLocalArq);
            this.tabArquivo.Controls.Add(this.lblCaminhoCert);
            this.tabArquivo.Controls.Add(this.txtCaminhoCert);
            this.tabArquivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabArquivo.ForeColor = System.Drawing.Color.Navy;
            this.tabArquivo.Location = new System.Drawing.Point(4, 26);
            this.tabArquivo.Name = "tabArquivo";
            this.tabArquivo.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabArquivo.Size = new System.Drawing.Size(298, 102);
            this.tabArquivo.TabIndex = 1;
            this.tabArquivo.Text = "Arquivo";
            // 
            // lblSenhaArquivo
            // 
            this.lblSenhaArquivo.AutoSize = true;
            this.lblSenhaArquivo.BackColor = System.Drawing.Color.Transparent;
            this.lblSenhaArquivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenhaArquivo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblSenhaArquivo.Location = new System.Drawing.Point(16, 54);
            this.lblSenhaArquivo.Name = "lblSenhaArquivo";
            this.lblSenhaArquivo.Size = new System.Drawing.Size(142, 16);
            this.lblSenhaArquivo.TabIndex = 18;
            this.lblSenhaArquivo.Text = "Senha do Certificado";
            this.lblSenhaArquivo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSenhaCert
            // 
            this.txtSenhaCert.BackColor = System.Drawing.Color.White;
            this.txtSenhaCert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenhaCert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenhaCert.ForeColor = System.Drawing.Color.Navy;
            this.txtSenhaCert.Location = new System.Drawing.Point(16, 71);
            this.txtSenhaCert.MaxLength = 50;
            this.txtSenhaCert.Name = "txtSenhaCert";
            this.txtSenhaCert.Size = new System.Drawing.Size(268, 22);
            this.txtSenhaCert.TabIndex = 19;
            this.txtSenhaCert.UseSystemPasswordChar = true;
            // 
            // btnLocalArq
            // 
            this.btnLocalArq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalArq.ForeColor = System.Drawing.Color.Navy;
            this.btnLocalArq.Location = new System.Drawing.Point(254, 27);
            this.btnLocalArq.Name = "btnLocalArq";
            this.btnLocalArq.Size = new System.Drawing.Size(30, 24);
            this.btnLocalArq.TabIndex = 17;
            this.btnLocalArq.Text = "...";
            this.btnLocalArq.UseVisualStyleBackColor = true;
            this.btnLocalArq.Click += new System.EventHandler(this.btnLocalArq_Click);
            // 
            // lblCaminhoCert
            // 
            this.lblCaminhoCert.AutoSize = true;
            this.lblCaminhoCert.BackColor = System.Drawing.Color.Transparent;
            this.lblCaminhoCert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaminhoCert.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCaminhoCert.Location = new System.Drawing.Point(16, 12);
            this.lblCaminhoCert.Name = "lblCaminhoCert";
            this.lblCaminhoCert.Size = new System.Drawing.Size(158, 16);
            this.lblCaminhoCert.TabIndex = 15;
            this.lblCaminhoCert.Text = "Caminho do Certificado";
            this.lblCaminhoCert.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCaminhoCert
            // 
            this.txtCaminhoCert.BackColor = System.Drawing.Color.White;
            this.txtCaminhoCert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCaminhoCert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaminhoCert.ForeColor = System.Drawing.Color.Navy;
            this.txtCaminhoCert.Location = new System.Drawing.Point(16, 28);
            this.txtCaminhoCert.MaxLength = 50;
            this.txtCaminhoCert.Name = "txtCaminhoCert";
            this.txtCaminhoCert.Size = new System.Drawing.Size(234, 22);
            this.txtCaminhoCert.TabIndex = 16;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.Color.Navy;
            this.btnSalvar.Location = new System.Drawing.Point(193, 144);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(121, 33);
            this.btnSalvar.TabIndex = 0;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // frmConfigCertificados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(319, 183);
            this.Controls.Add(tabCertificado);
            this.Controls.Add(this.btnSalvar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfigCertificados";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuração do Certificado";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmConfigNFCe_Load);
            tabCertificado.ResumeLayout(false);
            this.tabInstalado.ResumeLayout(false);
            this.tabInstalado.PerformLayout();
            this.tabArquivo.ResumeLayout(false);
            this.tabArquivo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TabPage tabInstalado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCdPIN;
        private System.Windows.Forms.Button btnPesqCert;
        private System.Windows.Forms.TextBox txtSerialCert;
        private System.Windows.Forms.TabPage tabArquivo;
        private System.Windows.Forms.Label lblSenhaArquivo;
        private System.Windows.Forms.TextBox txtSenhaCert;
        private System.Windows.Forms.Button btnLocalArq;
        private System.Windows.Forms.Label lblCaminhoCert;
        private System.Windows.Forms.TextBox txtCaminhoCert;
    }
}