﻿using AssinadorDeXml.utils;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace PDVNFCe.Forms
{
    public partial class frmCertificados : Form, IDisposable
    {
        private string strCertificado = "";

        public string Certificado
        {
            get { return strCertificado; }
        }

        public frmCertificados()
        {
            InitializeComponent();
        }

        private void frmCertificados_Load(object sender, EventArgs e)
        {
            X509Store stores = new X509Store(StoreName.My, StoreLocation.CurrentUser);

            stores.Open(OpenFlags.ReadOnly);

            X509Certificate2Collection certificados = stores.Certificates;

            grvCertificados.Rows.Clear();
            int i = 0;

            foreach (X509Certificate2 certificado in certificados)
            {
                string strSerial = certificado.SerialNumber;
                string strNome = certificado.Subject.Split(',')[0].ToString().Replace("CN=", "");
                string strEmissor = certificado.Issuer.Split(',')[0].ToString().Replace("CN=", "");
                string strDtIni = certificado.NotBefore.FormatarData("dd/MM/yyyy");
                string strDtFim = certificado.NotAfter.FormatarData("dd/MM/yyyy");

                grvCertificados.Rows.Add();
                grvCertificados[1, i].Value = strSerial;
                grvCertificados[2, i].Value = "   " + strNome + Environment.NewLine +
                                              "   Emissor: " + strEmissor + Environment.NewLine +
                                              "   Valido de " + strDtIni + " a " + strDtFim;
                i++;
            }

            grvCertificados.Refresh();
            stores.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (strCertificado.Trim() == "")
            {
                Funcoes.Advertencia("Selecione um certificado.");
                return;
            }

            this.Dispose(true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            strCertificado = "";
            this.Dispose(true);
        }

        private void grvCertificados_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            strCertificado = "";

            if (e.RowIndex < 0) return;

            if (grvCertificados[1, e.RowIndex].Value != null && grvCertificados[1, e.RowIndex].Value.ToString() != "")
                strCertificado = grvCertificados[1, e.RowIndex].Value.ToString() + "";
        }

        void IDisposable.Dispose()
        {
            this.Dispose(true);
        }
    }
}
