﻿namespace AssinadorDeXml.forms
{
    partial class frmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.txtCaminhoXml = new System.Windows.Forms.TextBox();
            this.lblCaminhoCert = new System.Windows.Forms.Label();
            this.btnLocalXml = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.btnAssinar = new System.Windows.Forms.Button();
            this.btnConfiguar = new System.Windows.Forms.Button();
            this.toolTipBotoes = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // txtCaminhoXml
            // 
            this.txtCaminhoXml.BackColor = System.Drawing.Color.White;
            this.txtCaminhoXml.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCaminhoXml.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaminhoXml.ForeColor = System.Drawing.Color.Black;
            this.txtCaminhoXml.Location = new System.Drawing.Point(12, 71);
            this.txtCaminhoXml.MaxLength = 50;
            this.txtCaminhoXml.Name = "txtCaminhoXml";
            this.txtCaminhoXml.Size = new System.Drawing.Size(332, 22);
            this.txtCaminhoXml.TabIndex = 19;
            // 
            // lblCaminhoCert
            // 
            this.lblCaminhoCert.AutoSize = true;
            this.lblCaminhoCert.BackColor = System.Drawing.Color.Transparent;
            this.lblCaminhoCert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaminhoCert.ForeColor = System.Drawing.Color.Black;
            this.lblCaminhoCert.Location = new System.Drawing.Point(12, 55);
            this.lblCaminhoCert.Name = "lblCaminhoCert";
            this.lblCaminhoCert.Size = new System.Drawing.Size(231, 16);
            this.lblCaminhoCert.TabIndex = 18;
            this.lblCaminhoCert.Text = "Selecione o xml que será assinado";
            this.lblCaminhoCert.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnLocalXml
            // 
            this.btnLocalXml.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalXml.ForeColor = System.Drawing.Color.Black;
            this.btnLocalXml.Location = new System.Drawing.Point(351, 70);
            this.btnLocalXml.Name = "btnLocalXml";
            this.btnLocalXml.Size = new System.Drawing.Size(30, 24);
            this.btnLocalXml.TabIndex = 20;
            this.btnLocalXml.Text = "...";
            this.btnLocalXml.UseVisualStyleBackColor = true;
            this.btnLocalXml.Click += new System.EventHandler(this.btnLocalXml_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.BackColor = System.Drawing.Color.DarkOrange;
            this.btnValidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidar.ForeColor = System.Drawing.Color.White;
            this.btnValidar.Location = new System.Drawing.Point(81, 138);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(147, 34);
            this.btnValidar.TabIndex = 21;
            this.btnValidar.Text = "Validar Assinatura";
            this.toolTipBotoes.SetToolTip(this.btnValidar, "Clique aqui caso queira validar se a assinatura do xml selecionado é válida.");
            this.btnValidar.UseVisualStyleBackColor = false;
            this.btnValidar.Visible = false;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // btnAssinar
            // 
            this.btnAssinar.BackColor = System.Drawing.Color.ForestGreen;
            this.btnAssinar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssinar.ForeColor = System.Drawing.Color.White;
            this.btnAssinar.Location = new System.Drawing.Point(234, 138);
            this.btnAssinar.Name = "btnAssinar";
            this.btnAssinar.Size = new System.Drawing.Size(147, 34);
            this.btnAssinar.TabIndex = 22;
            this.btnAssinar.Text = "Assinar Xml";
            this.toolTipBotoes.SetToolTip(this.btnAssinar, "Clique aqui para assinar o xml selecionado.");
            this.btnAssinar.UseVisualStyleBackColor = false;
            this.btnAssinar.Click += new System.EventHandler(this.btnAssinar_Click);
            // 
            // btnConfiguar
            // 
            this.btnConfiguar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnConfiguar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnConfiguar.Image = global::AssinadorDeXml.Properties.Resources.icon__1_;
            this.btnConfiguar.Location = new System.Drawing.Point(333, 10);
            this.btnConfiguar.Name = "btnConfiguar";
            this.btnConfiguar.Size = new System.Drawing.Size(48, 34);
            this.btnConfiguar.TabIndex = 1;
            this.toolTipBotoes.SetToolTip(this.btnConfiguar, "Configure os dados do certificado que será utilizado para assinar o xml.\r\nPoderão" +
        " ser utilizados certificados instalados ou aquivos de certificados \r\ncom chave p" +
        "rivada acessíveis.");
            this.btnConfiguar.UseVisualStyleBackColor = true;
            this.btnConfiguar.Click += new System.EventHandler(this.btnConfigurar_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 184);
            this.Controls.Add(this.btnAssinar);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.btnLocalXml);
            this.Controls.Add(this.lblCaminhoCert);
            this.Controls.Add(this.txtCaminhoXml);
            this.Controls.Add(this.btnConfiguar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Assinador de Xmls";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnConfiguar;
        private System.Windows.Forms.TextBox txtCaminhoXml;
        private System.Windows.Forms.Label lblCaminhoCert;
        private System.Windows.Forms.Button btnLocalXml;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.Button btnAssinar;
        private System.Windows.Forms.ToolTip toolTipBotoes;
    }
}

