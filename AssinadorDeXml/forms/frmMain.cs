﻿using AssinadorDeXml.utils;
using AssinadorDeXml.utils.objetos;
using PDVNFCe.Forms;
using System;
using System.IO;
using System.Windows.Forms;

namespace AssinadorDeXml.forms
{
    public partial class frmMain : Form
    {
        FuncoesDeCertificado fc;
        AssinadorConfig configXml;

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                configXml = Funcoes.GetAssinadorConfig();
                frmConfigCertificados cert = new frmConfigCertificados();
                cert.ShowDialog(this);
                configXml = Funcoes.GetAssinadorConfig();
                if (configXml != null)
                {
                    configXml.LastSelectedFile = txtCaminhoXml.Text;
                    Funcoes.SetConfig(configXml);
                }
            }
            catch (Exception ex)
            {
                Funcoes.Critica("Ocorreu um erro inesperado.\n" + ex.Message);
            }
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {

        }

        private void btnAssinar_Click(object sender, EventArgs e)
        {
            configXml = Funcoes.GetAssinadorConfig();
            if (configXml == null)
            {
                Funcoes.Critica("Nenhuma configuração localizada para gerar a assinatura do xml.");
                return;
            }
            if (String.IsNullOrEmpty(configXml.LastSelectedFile))
            {
                Funcoes.Critica("Informe o caminho do arquivo à ser assinado.");
                return;
            }

            var serialCert = configXml.Config.Instalado.NumeroSerialCertificado;
            var pinCert = configXml.Config.Instalado.SenhaCertificado;
            var caminhoCert = configXml.Config.Arquivo.CaminhoCertificado;
            var senhaCert = configXml.Config.Arquivo.SenhaCertificado;

            if (String.IsNullOrEmpty(serialCert) && String.IsNullOrEmpty(caminhoCert))
            {
                Funcoes.Critica("Nenhum certificado configurado./nFavor configurar um certificado para prosseguir.");
                return;
            }
            else if (String.IsNullOrEmpty(senhaCert) && String.IsNullOrEmpty(pinCert))
            {
                Funcoes.Critica("Senha do certificado não configurada./nFavor configurar um certificado e uma senha válida para prosseguir.");
                return;
            }

            fc.x509Cert = null;
            if (!String.IsNullOrEmpty(serialCert))
            {
                fc.x509Cert = fc.GetCertificado(configXml.Config.Instalado.NumeroSerialCertificado);
                fc.CertificadoPIN = pinCert;
            }
            else if (!String.IsNullOrEmpty(caminhoCert))
            {
                fc.x509Cert = fc.GetCertificadoArquivo(caminhoCert, senhaCert);
                fc.CertificadoPIN = senhaCert;
            }

            if (fc.x509Cert == null)
            {
                Funcoes.Critica("Certificado não localizado com os dados fornecidos.\nFavor revisar a configuração do certificado para prosseguir.");
                return;
            }
            var arquivoOrigem = configXml.LastSelectedFile;
            var arquivoDestino = arquivoOrigem.ToUpper().Replace(".XML", "-sng.xml");
            fc.AssinarXML(arquivoOrigem, "", arquivoDestino);
            if (File.Exists(arquivoDestino))
            {
                var resultado = Funcoes.Pergunta("O arquivo assinado pode ser encontrado no caminho:\n\n" + arquivoDestino +"\n\nDeseja abrir a pasta do arquivo?");
                if(resultado == DialogResult.Yes)
                {
                    Funcoes.AbrirPastaComArquivoSelecionado(arquivoDestino);
                }
            }
            else
            {
                Funcoes.Advertencia("O arquivo não pode ser gerado. Execute a aplicação como administrador, revise as configurações e tente novamente.");
            }
            return;
        }

    private void btnLocalXml_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "All Files|*.xml";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtCaminhoXml.Text = openFileDialog1.FileName;
                var configXml = Funcoes.GetAssinadorConfig();
                configXml.LastSelectedFile = txtCaminhoXml.Text;
                Funcoes.SetConfig(configXml);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                fc = new FuncoesDeCertificado();
                configXml = Funcoes.GetAssinadorConfig();
                if (configXml != null)
                {
                    if (!String.IsNullOrEmpty(configXml.LastSelectedFile))
                    {
                        txtCaminhoXml.Text = configXml.LastSelectedFile;
                    }
                }
            }
            catch (Exception ex)
            {
                Funcoes.Critica("Ocorreu um erro ao abrir a aplicação.\n" + ex.Message);
            }
        }
    }
}
