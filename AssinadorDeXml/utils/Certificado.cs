﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace AssinadorDeXml.utils
{
    class FuncoesDeCertificado
    {
        /// <summary>
        /// Certificado digital
        /// </summary>
        public X509Certificate2 x509Cert { get; set; }
        /// <summary>
        /// Type do Provider do Certificado selecionado
        /// </summary>
        public string ProviderTypeCertificado { get; set; }
        /// <summary>
        /// Provider utilizado pelo certificado se utilizar a opção para salvar o PIN
        /// </summary>
        public string ProviderCertificado { get; set; }

        /// <summary>
        /// PIN do certificado
        /// </summary>
        public string CertificadoPIN { get; set; }


        #region AssinarXML
        /// <summary>
        /// Assina o XML e sobrepondo-o
        /// </summary>
        /// <param name="sDirXML">Nome do arquivo XML a ser assinado</param>
        /// <param name="sTagAssinatura">Nome da tag onde é para ficar a assinatura</param>
        public void AssinarXML(string strDirXML, string sTagAssinatura, string arquivoDestino)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(strDirXML);
            if (String.IsNullOrEmpty(sTagAssinatura))
            {
                if (doc.FirstChild.InnerText.ToLower().Contains("encoding"))
                {
                    sTagAssinatura = doc.ChildNodes[1].Name;
                }
            }
            if (!String.IsNullOrEmpty(sTagAssinatura))
            {
                if (!Assinado(doc, sTagAssinatura))
                    this.Assinar(doc, sTagAssinatura, "", arquivoDestino);
            }
        }
        #endregion

        #region Assinar
        /// <summary>
        /// O método assina digitalmente o arquivo XML passado por parâmetro e 
        /// grava o XML assinado com o mesmo nome, sobreponto o XML informado por parâmetro.
        /// Disponibiliza também uma propriedade com uma string do xml assinado (this.vXmlStringAssinado)
        /// </summary>
        /// <param name="sDirXML">Diretorio do arquivo XML a ser assinado</param>
        /// <param name="sTagAssinatura">Nome da tag onde é para ficar a assinatura</param>
        /// /// <param name="sTagAtributo">Nome da tag que vai ser assinada</param>
        private void Assinar(string sDirXML, string sTagAssinatura, string sTagAtributo)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.PreserveWhitespace = false;
                xmlDoc.Load(sDirXML);

                // Gravar o XML Assinado no HD
                using (StreamWriter sw = File.CreateText(sDirXML))
                {
                    sw.Write(Assinar(xmlDoc, sTagAssinatura, sTagAtributo));
                    sw.Close();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Assinar(XmlDocument xmlDoc, string sTagAssinatura, string sTagAtributo, string novoArc)
        {
            try
            {
                xmlDoc.PreserveWhitespace = false;
                // Gravar o XML Assinado no HD
                using (StreamWriter sw = File.CreateText(novoArc))
                {
                    sw.Write(Assinar(xmlDoc, sTagAssinatura, sTagAtributo));
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// O método assina digitalmente o arquivo XML passado por parâmetro e 
        /// grava o XML assinado com o mesmo nome, sobreponto o XML informado por parâmetro.
        /// Disponibiliza também uma propriedade com uma string do xml assinado (this.vXmlStringAssinado)
        /// </summary>
        /// <param name="xml">arquivo XML a ser assinado</param>
        /// <param name="sTagAssinatura">Nome da tag onde é para ficar a assinatura</param>
        /// /// <param name="sTagAtributo">Nome da tag que vai ser assinada</param>
        private string Assinar(XmlDocument xmlDoc, string sTagAssinatura, string sTagAtributo)
        {
            try
            {
                if (xmlDoc.GetElementsByTagName(sTagAssinatura).Count == 0)
                {
                    throw new Exception("A tag de assinatura " + sTagAssinatura.Trim() + " não existe no XML. (Código do Erro: 5)");
                }
                //else if (xmlDoc.GetElementsByTagName(sTagAtributo).Count == 0)
                //{
                //    throw new Exception("A tag de assinatura " + sTagAtributo.Trim() + " não existe no XML. (Código do Erro: 4)");
                //}
                else
                {
                    XmlDocument xmlAss;
                    xmlDoc.PreserveWhitespace = false;
                    XmlNodeList lists = xmlDoc.GetElementsByTagName(sTagAssinatura);

                    foreach (XmlNode nodes in lists)
                    {
                        if (string.IsNullOrEmpty(sTagAtributo))
                        {
                            sTagAtributo = nodes.FirstChild.Name;
                        }
                        foreach (XmlNode childNodes in nodes.ChildNodes)
                        {
                            if (!childNodes.Name.Equals(sTagAtributo))
                                break;
                           
                            Reference reference = new Reference();
                            reference.Uri = "";

                            //pega o uri que deve ser assinada                                       
                            XmlElement childElemen = (XmlElement)childNodes;

                            // Create a SignedXml object.
                            SignedXml signedXml = new SignedXml(xmlDoc);

                            //A3
                            if (!String.IsNullOrEmpty(CertificadoPIN) && IsA3(x509Cert))
                            {
                                signedXml.SigningKey = LerDispositivo(CertificadoPIN, Convert.ToInt32(this.ProviderTypeCertificado), this.ProviderCertificado);
                            }
                            else
                            {
                                signedXml.SigningKey = this.x509Cert.PrivateKey;
                            }

                            // Add an enveloped transformation to the reference.
                            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                            reference.AddTransform(env);

                            XmlDsigC14NTransform c14 = new XmlDsigC14NTransform();
                            reference.AddTransform(c14);

                            // Add the reference to the SignedXml object.
                            signedXml.AddReference(reference);

                            // Create a new KeyInfo object
                            KeyInfo keyInfo = new KeyInfo();

                            // Load the certificate into a KeyInfoX509Data object
                            // and add it to the KeyInfo object.
                            keyInfo.AddClause(new KeyInfoX509Data(this.x509Cert));

                            // Add the KeyInfo object to the SignedXml object.
                            signedXml.KeyInfo = keyInfo;
                            signedXml.ComputeSignature();

                            // Get the XML representation of the signature and save
                            // it to an XmlElement object.
                            XmlElement xmlDigitalSignature = signedXml.GetXml();

                            //Gravar o elemento no documento XML
                            nodes.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));
                        }
                    }

                    xmlAss = new XmlDocument();
                    xmlAss.PreserveWhitespace = false;
                    xmlAss = xmlDoc;

                    // Atualizar a string do XML já assinada
                    return xmlAss.OuterXml;
                }
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                throw new Exception("O certificado deverá ser reiniciado.\r\n Retire o certificado.\r\nAguarde o LED terminar de piscar.\r\n Recoloque o certificado e informe o PIN novamente.\r\n" + ex.Message.ToString());
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region IsA3
        /// <summary>
        /// Retorna true se o certificado for do tipo A3.
        /// </summary>
        /// <param name="_x509cert">Certificado que deverá ser validado se é A3 ou não.</param>
        private bool IsA3(X509Certificate2 _x509cert)
        {
            if (_x509cert == null)
                return false;

            bool result = false;

            try
            {
                RSACryptoServiceProvider service = _x509cert.PrivateKey as RSACryptoServiceProvider;

                if (service != null)
                {
                    if (service.CspKeyContainerInfo.Removable &&
                    service.CspKeyContainerInfo.HardwareDevice)
                        result = true;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }
        #endregion

        #region LerDispositivo
        /// <summary>
        /// Ler o dispositivo certificado A3
        /// </summary>
        /// <param name="PIN">Codigo PIN do certificado</param>
        /// <param name="providerType">Type do Provider do Certificado selecionado</param>
        /// <param name="provider">Provider utilizado pelo certificado se utilizar a opção para salvar o PIN</param>
        private RSACryptoServiceProvider LerDispositivo(string PIN, int providerType, string provider)
        {
            CspParameters csp = new CspParameters(providerType, provider);

            SecureString ss = new SecureString();

            foreach (char a in PIN)
            {
                ss.AppendChar(a);
            }

            csp.KeyPassword = ss;
            csp.KeyNumber = 1;
            csp.Flags = CspProviderFlags.NoPrompt;

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(csp);
            rsa.PersistKeyInCsp = false;
            return rsa;
        }
        #endregion

        #region Assinado
        /// <summary>
        /// Verificar se o XML já tem assinatura
        /// </summary>
        /// <param name="sDirXML">Arquivo XML a ser verificado se tem assinatura</param>
        /// <param name="sTagAssinatura">Tag de assinatura onde vamos pesquisar</param>
        private bool Assinado(string sDirXML, string sTagAssinatura)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(sDirXML);
            if (String.IsNullOrEmpty(sTagAssinatura))
            {
                if (doc.FirstChild.InnerText.ToLower().Contains("encoding"))
                {
                    sTagAssinatura = doc.ChildNodes[1].Name;
                }
            }

            return Assinado(doc, sTagAssinatura);
        }

        /// <summary>
        /// Verificar se o XML já tem assinatura
        /// </summary>
        /// <param name="doc">Arquivo XML a ser verificado se tem assinatura</param>
        /// <param name="sTagAssinatura">Tag de assinatura onde vamos pesquisar</param>
        private bool Assinado(XmlDocument doc, string sTagAssinatura)
        {
            bool retorno = false;

            if (doc.GetElementsByTagName(sTagAssinatura)[0].LastChild.Name == "Signature")
                retorno = true;

            return retorno;
        }
        #endregion

        #region GetCertificado
        /// <summary>
        /// Busca o certificado pelo numero do serial
        /// </summary>
        /// <param name="sSerialNumber">Numero serial do certificado</param>
        public X509Certificate2 GetCertificado(string sSerialNumber)
        {
            X509Store stores = new X509Store(StoreName.My, StoreLocation.CurrentUser);

            stores.Open(OpenFlags.ReadOnly);

            X509Certificate2Collection certificados = stores.Certificates;

            foreach (X509Certificate2 certificado in certificados)
            {
                if (certificado.GetSerialNumberString().ToLower() == ("" + sSerialNumber).Trim().ToLower())
                {
                    stores.Close();
                    return certificado;
                }
            }

            stores.Close();
            throw new Exception("Certificado não encontrado. \n" + "Numero do serial: " + sSerialNumber);
        }
        /// <summary>
        /// Busca o certificado pelo caminho do arquivo
        /// </summary>
        /// <param name="caminhoArquivo">Caminho do arquivo do certificado</param>
        /// /// <param name="senha">Senha do certificado</param>
        public X509Certificate2 GetCertificadoArquivo(string caminhoArquivo, string senha)
        {

            FileStream fs = new FileStream(caminhoArquivo, FileMode.Open);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            fs.Dispose();
            X509Certificate2 cert = new X509Certificate2(buffer, senha);
            return cert;
        }
        #endregion
    }
}
