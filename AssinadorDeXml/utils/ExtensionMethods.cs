﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace AssinadorDeXml.utils
{
    public static class ExtensionMethods
    {
        public static bool IsNumeric(this object objeto)
        {
            if (objeto == null)
                objeto = false;

            double dblResult;
            return double.TryParse(objeto.ToString(), out dblResult);
        }

        public static bool IsDate(this object objeto)
        {
            DateTime dtRsult;
            return DateTime.TryParse(objeto.ToString(), out dtRsult);
        }

        public static string Right(this string strTexto, int intLen)
        {
            if (strTexto == null)
                return "";

            if (intLen > strTexto.Length)
            {
                return strTexto;
            }
            else
            {
                return strTexto.Substring(strTexto.Length - intLen, intLen);
            }
        }

        public static string Left(this string strTexto, int intLen)
        {
            if (strTexto == null)
                return "";

            if (intLen > strTexto.Length)
            {
                return strTexto;
            }
            else
            {
                return strTexto.Substring(0, intLen);
            }
        }

        public static string Mid(this string strTexto, int intStart,int intLen)
        {
            if (strTexto == null)
                return "";

            if (intLen > strTexto.Length)
            {
                return strTexto;
            }
            else
            {
                return strTexto.Substring(intStart, intLen);
            }
        }

        public static int ToInt32(this object objeto)
        {
            int intResult;
            return int.TryParse(objeto.ToString(), out intResult) ? intResult : 0;
        }

        public static long ToLong(this object objeto)
        {
            long lngResult;
            return long.TryParse(objeto.ToString(), out lngResult) ? lngResult : 0;
        }

        public static double ToDouble(this object objeto)
        {
            double dblResult;
            return double.TryParse(objeto.ToString(), out dblResult) ? dblResult : 0;
        }

        public static DateTime ToDate(this object objeto)
        {
            DateTime dblResult;

            try
            {
                dblResult = DateTime.Parse(objeto.ToString());
            }
            catch (Exception)
            {
                throw new Exception("Não e possível converter objeto para tipo Data");
            }

            return dblResult;
        }

        public static string Formatar(this double objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", objeto);
        }

        public static string ToStr(this double objeto)
        {
            return objeto.ToString().Replace(',','.');  
        }

        public static string FormatarTexto(this string objeto, int iQtdeVezes, Char sCaracterFormatacao)
        {
            return objeto.PadLeft(iQtdeVezes, sCaracterFormatacao);
        }

        public static string Formatar(this long objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", objeto);
        }

        public static string Formatar(this string objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", objeto);
        }

        public static string Formatar(this int objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", objeto);
        }

        public static string FormatarData(this DateTime objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", objeto);
        }

        public static string FormatarData(this object objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", Convert.ToDateTime(objeto));
        }

        public static string FormatarData(this string  objeto, string sFormato)
        {
            return String.Format("{0:" + sFormato + "}", Convert.ToDateTime(objeto));
        }

        public static string strReverse(this string objeto)
        {
            char[] chr = objeto.ToCharArray();
            Array.Reverse(chr);
            return new string(chr);
        }

        public static bool ToBool(this object objeto)
        {
            if (!objeto.IsNumeric())
                return false;

            return Convert.ToBoolean(objeto.ToInt32());
        }

        public static void Stop(this object objeto)
        {
            System.Diagnostics.Debugger.Break();
        }

        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();

            string name = Enum.GetName(type, value);

            if (name != null)
            {
                FieldInfo field = type.GetField(name);

                if (field != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;

                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }

            return "";
        }

 
    }
}

