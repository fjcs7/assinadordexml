﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Windows.Forms;
using AssinadorDeXml.utils.objetos;
using System.Text;
using System.Diagnostics;

namespace AssinadorDeXml.utils
{
    public static class Funcoes
    {
        public static string ArqConfigPath { get { return Environment.CurrentDirectory + "/AssinadorConfig.xml"; } }

        public static string Versao { get { return "Versão: " + Application.ProductVersion; } }

        public static void Critica(string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(strMensagem, "Crítica - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Error);  //Form.ActiveForm
            Application.DoEvents();
        }

        public static void Critica(string strOrigem, string strMensagem)
        {
            string strCabecalho = strOrigem + " - " + "Critica";
            Application.DoEvents();
            MessageBox.Show(strMensagem, strCabecalho + " - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Error);  //Form.ActiveForm
            Application.DoEvents();
        }

        public static void Critica(IWin32Window formowner, string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(formowner, strMensagem, "Crítica - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Application.DoEvents();
        }
        public static void Critica(string strMensagem, Exception ex)
        {
            Application.DoEvents();
            MessageBox.Show(strMensagem, "Crítica - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Application.DoEvents();
        }

        public static void Informacao(string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(strMensagem, "Informação - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.DoEvents();
        }
        public static void Informacao(Form frmOwner, string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(frmOwner, strMensagem, "Informação - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.DoEvents();
        }

        public static void Advertencia(string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(strMensagem, "Advertência - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Application.DoEvents();
        }

        public static void Advertencia(string strMensagem, Exception ex)
        {
            Application.DoEvents();
            MessageBox.Show(strMensagem, "Advertência - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Application.DoEvents();
        }
        public static void Advertencia(Form frmOwner, string strMensagem)
        {
            Application.DoEvents();
            MessageBox.Show(frmOwner, strMensagem, "Advertência - " + Versao, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Application.DoEvents();
        }
        public static DialogResult Pergunta(string strMensagem, bool booMarcaBtNao = false)
        {
            Application.DoEvents();
            DialogResult dlgResult;

            if (booMarcaBtNao)
            {
                dlgResult = MessageBox.Show(strMensagem, "Pergunta - " + Versao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            }
            else
            {
                dlgResult = MessageBox.Show(strMensagem, "Pergunta - " + Versao, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            }
            Application.DoEvents();

            return dlgResult;
        }
        public static DialogResult Pergunta(Form frmOwner, string strMensagem, bool booMarcaBtNao = false)
        {
            Application.DoEvents();
            DialogResult dlgResult;

            if (booMarcaBtNao)
            {
                dlgResult = MessageBox.Show(frmOwner, strMensagem, "Pergunta - " + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            }
            else
            {
                dlgResult = MessageBox.Show(frmOwner, strMensagem, "Pergunta - " + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            }

            Application.DoEvents();

            return dlgResult;
        }

        public static string getHostName()
        {
            string sHostName = Environment.MachineName;

            if (sHostName.Trim().Length == 0)
                return "";

            return sHostName;
        }

        public static string GetConfig(string strSection, string strTag)
        {
            XmlDocument xmlConfig = new XmlDocument();
            XmlNode nodeConfig;

            if (!File.Exists(ArqConfigPath))
                return "";

            xmlConfig.Load(ArqConfigPath);

            nodeConfig = xmlConfig.SelectSingleNode("configuration");

            if (nodeConfig == null)
                return "";

            if (nodeConfig.SelectSingleNode(strSection) != null)
            {
                if (nodeConfig.SelectSingleNode(strSection).SelectSingleNode(strTag) != null)
                {
                    return nodeConfig.SelectSingleNode(strSection).SelectSingleNode(strTag).InnerText.Trim();
                }
            }

            return "";
        }

        public static AssinadorConfig GetAssinadorConfig()
        {
            AssinadorConfig config = null;
            XmlSerializer serializer = new XmlSerializer(typeof(AssinadorConfig));
            if (File.Exists(ArqConfigPath))
            {
                using (FileStream configXmlFile = new FileStream(ArqConfigPath, FileMode.Open))
                {
                    config = (AssinadorConfig)serializer.Deserialize(configXmlFile);
                }
            }
            return config;
        }

        public static void SetConfig(string strSection, string strTag, string strValue)
        {
            XmlDocument xmlConfig = new XmlDocument();
            XmlNode nodeConfig;
            XmlNode nodeSection;
            XmlNode nodeTag;

            if (!File.Exists(ArqConfigPath))
            {
                xmlConfig.AppendChild(xmlConfig.CreateXmlDeclaration("1.0", "UTF-8", null));
            }
            else
            {
                xmlConfig.Load(ArqConfigPath);
            }

            nodeConfig = xmlConfig.SelectSingleNode("configuration");

            if (nodeConfig == null)
            {
                xmlConfig.AppendChild(xmlConfig.CreateElement("", "configuration", ""));
                nodeConfig = xmlConfig.SelectSingleNode("configuration");
            }

            nodeSection = nodeConfig.SelectSingleNode(strSection);

            if (nodeSection == null)
            {
                nodeConfig.AppendChild(xmlConfig.CreateElement("configuration", strSection, ""));
                xmlConfig.AppendChild(nodeConfig);
                nodeSection = nodeConfig.SelectSingleNode(strSection);
            }

            nodeTag = nodeConfig.SelectSingleNode(strSection).SelectSingleNode(strTag);

            if (nodeTag == null)
            {
                nodeSection.AppendChild(xmlConfig.CreateElement(strSection, strTag, ""));
                nodeTag = nodeSection.SelectSingleNode(strTag);
            }

            nodeTag.InnerText = strValue;

            xmlConfig.Save(ArqConfigPath);

            return;
        }

        public static void SetConfig(AssinadorConfig config)
        {
            XmlDocument xmlConfig = ParseObjetoToXML(config);
            xmlConfig.Save(ArqConfigPath);
        }
        private static XmlDocument ParseObjetoToXML(object objeto)
        {
            string sXMLString = string.Empty;

            XmlSerializer serializer = new XmlSerializer(objeto.GetType());
            XmlSerializerNamespaces xNamespaces = new XmlSerializerNamespaces();
            xNamespaces.Add("", "");

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            settings.OmitXmlDeclaration = false;

            using (StringWriter textWriter = new StringWriterUTF8())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(textWriter, objeto, xNamespaces);
                }

                sXMLString = textWriter.ToString();
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sXMLString);

            return xmlDoc;
        }

        public static string Criptografa(string texto)
        {
            return SecurityEncripter.Encrypt(texto);
        }
        public static string Decriptografa(string texto)
        {
            return SecurityEncripter.Decrypt(texto);
        }

        public static string LimpaNumeros(string strText)
        {
            int intPosicao;
            string strLetra;
            string strResultado;

            strResultado = "";
            intPosicao = 0;

            while (intPosicao < strText.Length)
            {
                strLetra = strText.Substring(intPosicao, 1);

                if (strLetra.IsNumeric())
                {
                    strResultado += strLetra;
                }

                intPosicao += 1;
            }

            return strResultado;
        }

        public static string MascaraCNPJCPF(string strCNPJCPF)
        {
            string _strCnpjCpf;

            _strCnpjCpf = Funcoes.LimpaNumeros(strCNPJCPF);

            if (_strCnpjCpf.Length == 11)
            {
                return string.Format(@"{0:000\.000\.000\-00}", _strCnpjCpf.ToLong());
            }
            else if (_strCnpjCpf.Length == 14)
            {
                return string.Format(@"{0:00\.000\.000\/0000\-00}", _strCnpjCpf.ToLong());
            }

            return "000.000.000-00";
        }

        private static bool DigitoVerificadorCGC(string CGC)
        {
            string strCaracter;
            string strcampo;
            string strConf;
            string strCGC;
            string strAux;
            double dblDivisao;
            long lngInteiro;
            int intNumero;
            int intSoma2;
            int intsoma1;
            int intSoma;
            int intResto;
            int intMais;
            int intDig1;
            int intDig2;

            strAux = LimpaNumeros(CGC);

            intSoma = 0;
            intsoma1 = 0;
            intSoma2 = 0;
            intNumero = 0;
            intMais = 0;

            strCGC = strAux.Right(6);
            strCGC = strCGC.Left(4);
            strcampo = strAux.Left(8);
            strcampo = strcampo.Right(4) + strCGC;

            for (int i = 2; i <= 9; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                intsoma1 += intMais;
            }

            strcampo = strAux.Left(4);

            for (int i = 2; i <= 5; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                intSoma2 += intMais;
            }

            intSoma = intsoma1 + intSoma2;
            dblDivisao = (double)intSoma / 11;
            lngInteiro = (int)dblDivisao * 11;
            intResto = (int)(intSoma - lngInteiro);

            if (intResto == 0 || intResto == 1)
            {
                intDig1 = 0;
            }
            else
            {
                intDig1 = 11 - intResto;
            }

            intSoma = 0;
            intsoma1 = 0;
            intSoma2 = 0;
            intNumero = 0;
            intMais = 0;

            strCGC = strAux.Right(6);
            strCGC = strCGC.Left(4);
            strcampo = strAux.Left(8);
            strcampo = strcampo.Right(3) + "" + strCGC + "" + intDig1;

            for (int i = 2; i <= 9; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                intsoma1 += intMais;
            }

            strcampo = strAux.Left(5);

            for (int i = 2; i <= 6; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                intSoma2 += intMais;
            }

            intSoma = intsoma1 + intSoma2;
            dblDivisao = (double)intSoma / 11;
            lngInteiro = (int)dblDivisao * 11;
            intResto = (int)(intSoma - lngInteiro);

            if (intResto == 0 || intResto == 1)
            {
                intDig2 = 0;
            }
            else
            {
                intDig2 = 11 - intResto;
            }

            strConf = intDig1.ToString() + "" + intDig2.ToString();

            if (strConf != strAux.Right(2))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool DigitoVerificadorCPF(string CPF)
        {
            string strCaracter;
            string strcampo;
            string strConf;
            string strAux;
            double dblDivisao;
            long lngInteiro;
            long lngSoma;
            int intNumero;
            int intResto;
            int intMais;
            int intDig1;
            int intDig2;

            strAux = LimpaNumeros(CPF);

            lngSoma = 0;
            intNumero = 0;
            intMais = 0;
            strcampo = strAux.Left(9);

            for (int i = 2; i <= 10; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                lngSoma += intMais;
            }

            dblDivisao = (double)lngSoma / 11;
            lngInteiro = (int)dblDivisao * 11;
            intResto = (int)(lngSoma - lngInteiro);

            if (intResto == 0 || intResto == 1)
            {
                intDig1 = 0;
            }
            else
            {
                intDig1 = 11 - intResto;
            }

            strcampo = strcampo + "" + intDig1;
            lngSoma = 0;
            intNumero = 0;
            intMais = 0;

            for (int i = 2; i <= 11; i++)
            {
                strCaracter = strcampo.Right(i - 1);
                intNumero = strCaracter.Left(1).ToInt32();
                intMais = intNumero * i;
                lngSoma += intMais;
            }

            dblDivisao = (double)lngSoma / 11;
            lngInteiro = (int)dblDivisao * 11;
            intResto = (int)(lngSoma - lngInteiro);

            if (intResto == 0 || intResto == 1)
            {
                intDig2 = 0;
            }
            else
            {
                intDig2 = 11 - intResto;
            }

            strConf = intDig1.ToString() + "" + intDig2.ToString();

            if (strConf != strAux.Right(2))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool ValidaCNPJCPF(string CGC_CPF, bool blnMsg = false)
        {
            string strCaracter;
            string strAux;
            bool blnValido;
            bool blnX;

            blnValido = false;

            if (CGC_CPF.Trim() != "")
            {
                strAux = LimpaNumeros(CGC_CPF);

                strCaracter = strAux.Substring(0, 1);

                for (int i = 1; i < strAux.Length; i++)
                {
                    if (strCaracter != strAux.Substring(i, 1))
                    {
                        blnValido = true;
                        break;
                    }

                    strCaracter = strAux.Substring(i, 1);
                }

                if (!blnValido)
                {
                    return false;
                }

                if (strAux.Length > 11)
                {
                    blnX = DigitoVerificadorCGC(CGC_CPF);
                }
                else
                {
                    blnX = DigitoVerificadorCPF(CGC_CPF);
                }
            }
            else
            {
                blnX = false;
            }

            if (blnX)
            {
                return true;
            }
            else if (blnX = false && blnMsg)
            {
                Critica("CGC ou CPF Incorreto, Por favor Verifique!");
            }
            else if (blnMsg)
            {
                Critica("Número incorreto de caracteres!");
            }

            return false;
        }

        public static T FromXml<T>(String xml)
        {
            T returnedXmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                    }
                }
            }
            catch (Exception)
            {

            }
            return returnedXmlClass;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static void AbrirPastaComArquivoSelecionado(string arquivoQueSeraSelecionado)
        {
            string args = string.Format("/Select, \"{0}\"", arquivoQueSeraSelecionado);
            ProcessStartInfo pfi = new ProcessStartInfo("Explorer.exe", args);
            System.Diagnostics.Process.Start(pfi);
        }
    }
}
