﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.IO
{
    class StringWriterUTF8: StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }
}
