﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AssinadorDeXml.utils.objetos
{
    public class AssinadorConfig
    {

        [XmlElement(ElementName = "configuration")]
        public Configuracao Config { get; set; }

        private string _LastSelectedFile;
        [XmlElement(ElementName = "last_file")]
        public string LastSelectedFile
        {
            get { return Funcoes.Decriptografa(_LastSelectedFile); }
            set { _LastSelectedFile = Funcoes.Criptografa(value); }
        }

        public AssinadorConfig()
        {
            LastSelectedFile = String.Empty;
            Config = new Configuracao();
        }

        public class Configuracao
        {
            public Configuracao()
            {
                Instalado = new DadosInstalados();
                Arquivo = new DadosArquivo();
            }
            [XmlElement(ElementName = "instalado")]
            public DadosInstalados Instalado { get; set; }
            [XmlElement(ElementName = "arquivo")]
            public DadosArquivo Arquivo { get; set; }
        }

        public class DadosInstalados
        {
            private string _NumeroSerialCertificado;
            private string _SenhaCertificado;

            [XmlElement(ElementName = "nsc")]
            public string NumeroSerialCertificado
            {
                get { return Funcoes.Decriptografa(_NumeroSerialCertificado); }
                set { _NumeroSerialCertificado = Funcoes.Criptografa(value); }
            }

            [XmlElement(ElementName = "psc")]
            public string SenhaCertificado
            {
                get { return Funcoes.Decriptografa(_SenhaCertificado); }
                set { _SenhaCertificado = Funcoes.Criptografa(value); }
            }
        }

        public class DadosArquivo
        {
            private string _CaminhoCertificado;
            private string _SenhaCertificado;

            [XmlElement(ElementName = "pcc")]
            public string CaminhoCertificado
            {
                get { return Funcoes.Decriptografa(_CaminhoCertificado); }
                set { _CaminhoCertificado = Funcoes.Criptografa(value); }
            }

            [XmlElement(ElementName = "psc")]
            public string SenhaCertificado
            {
                get { return Funcoes.Decriptografa(_SenhaCertificado); }
                set { _SenhaCertificado = Funcoes.Criptografa(value); }
            }
        }
    }
}